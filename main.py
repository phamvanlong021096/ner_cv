#!/usr/bin/env python
# -*- coding: utf-8 -*-
import docx2txt
from Tkinter import Tk
from tkFileDialog import askopenfilename
from model_ner import NER_Model
from utils import get_config

def read_resume_docx(path):
    text = docx2txt.process(path)
    lines = text.split('\n')
    lines = [l.strip() + '\n' for l in lines if l != '']
    return lines


if __name__ == '__main__':
    # open choose file
    print "Choose file..."
    Tk().withdraw()
    file_path = askopenfilename()
    print "File choosen: " + file_path

    print "Reload model..."
    configs = get_config()
    model = NER_Model()

    print "Reload done! Predicting..."
    lines = read_resume_docx(file_path)
    for line in lines:
        output = model.predict(line, configs, display_format='CoNLL')
        print output
